package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class CheckWin {
    
    public CheckWin() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkForWinnerRow(){
        String[][] tablePlay = Lab03.table;
        String currentPlayer = "O";
        for(int i=0;i<3;i++){
            tablePlay[2][i] = "O";
        }
        boolean result = Lab03.checkWin(tablePlay, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void checkForWinnerCol(){
        String[][] tablePlay = Lab03.table;
        String currentPlayer = "O";
        for(int i=0;i<3;i++){
            tablePlay[i][0] = "O";
        }
        boolean result = Lab03.checkWin(tablePlay, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void checkForWinnerDiagonallyleft(){
        String[][] tablePlay = Lab03.table;
        String currentPlayer = "O";
        tablePlay[0][0] = "O";
        tablePlay[1][1] = "O";
        tablePlay[2][2] = "O";
        boolean result = Lab03.checkWin(tablePlay, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void checkForWinnerDiagonallyRight(){
        String[][] tablePlay = Lab03.table;
        String currentPlayer = "O";
        tablePlay[0][2] = "O";
        tablePlay[1][1] = "O";
        tablePlay[2][0] = "O";
        boolean result = Lab03.checkWin(tablePlay, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void checkForDraw(){
        String[][] tablePlay = Lab03.table;
        for(int row=0;row<3;row++){
            for(int col=0;col<3;col++){
                if((row+col)%2 == 0){
                    tablePlay[row][col] = "X";
                }else{
                    tablePlay[row][col] = "O";
                }
            }
        }
        //tablePlay[2][1] = "-"; comment if not draw or it doesn't finished final turn.
        
        boolean result = Lab03.checkDraw(tablePlay);
        assertEquals(true, result);
        
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
}

