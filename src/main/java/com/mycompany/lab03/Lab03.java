/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author acer
 */


public class Lab03 {

    static boolean checkWin(String[][] tablePlay, String currentPlayer) {
       for(int i=0;i<3;i++){
               if(tablePlay[i][0].equals(currentPlayer) && tablePlay[i][1].equals(currentPlayer) && tablePlay[i][2].equals(currentPlayer)){
                   return true;
               }else if(tablePlay[0][i].equals(currentPlayer) && tablePlay[1][i].equals(currentPlayer) && tablePlay[2][i].equals(currentPlayer)){
                   return true;
               }
           }
       if(tablePlay[0][0].equals(currentPlayer) && tablePlay[1][1].equals(currentPlayer) && tablePlay[2][2].equals(currentPlayer)){
            return true;
        }
       else if(tablePlay[2][0].equals(currentPlayer) && tablePlay[1][1].equals(currentPlayer) && tablePlay[2][0].equals(currentPlayer)){
            return true;
        }else{
            return false;
        }
    }
    static String[][] table = {{"-", "-", "-" },{"-", "-", "-" },{"-", "-", "-" }};

    static boolean checkWinTest(String[][] tablePlay, String currentPlayer) {
        for(int i=0;i<3;i++){
               if(tablePlay[i][0].equals(currentPlayer) && tablePlay[i][1].equals(currentPlayer) && tablePlay[i][2].equals(currentPlayer)){
                   return true;
               }
        }
        return false;
    }

    static boolean checkDraw(String[][] tablePlay) {
        int count = 0;
        for(int row=0;row<3;row++){
            for(int col=0;col<3;col++){
                if(tablePlay[row][col].equals("O") || tablePlay[row][col].equals("X")){
                    count++;
                    }
            }
        }
        if(count == 9){
            return true;
        }else{
            return false;
        }
    }
}


